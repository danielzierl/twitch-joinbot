//if you read this, join my channel (#zneix) and say hi :)
// const { ChatClient } = require('dank-twitch-irc');
// const chalk = require('chalk');
// const fs = require('fs');
//
// const util = require('./util.js');
// const config = require('./config.json');
import { ChatClient } from 'dank-twitch-irc';
import chalk from 'chalk';
import fs from 'fs';
import * as util from './util.js';
// import config from './config.json';


export const init_join_client = async (channels_to_join_fd, app_config) => {
  const config = JSON.parse(fs.readFileSync('config.json'));
  console.log(config);
  const client = new ChatClient({
    username: config.username,
    password: `oauth:${config.token}`,
  });
  client.connect();
  client.on('JOIN', context => {
    util.logTime(`${chalk.blueBright('[JOIN]')} ${chalk.whiteBright(context.channelName)}`, 2);
  });

  client.on('PART', context => {
    util.logTime(`${chalk.blue('[PART]')} ${chalk.white(context.channelName)}`, 2);
  });

  client.on('ready', async () => {
    util.logTime(`${chalk.green('[CONNECTED]')} || Connected to twitch.`, 0);
    const join_channels_callback = async () => {
      const start_time = Date.now();
      const channels_to_join = fs.readFileSync(channels_to_join_fd).toString().split('\n').filter(c => c).map(c => c.trim().toLowerCase());
      if (!channels_to_join.length) return util.logTime(`${chalk.red('[ERROR]')} No channels specified in channels.txt!`);
      util.logTime(`${channels_to_join.length} channels`);
      let joined_channels = 0;

      for (let i = 0; i < channels_to_join.length; i += app_config.JOIN_LIMIT) {

        await client.joinAll(channels_to_join.slice(i, i + app_config.JOIN_LIMIT));
        console.log("Waiting...")
        joined_channels += app_config.JOIN_LIMIT;
        if (Date.now() - start_time + app_config.TWITCH_JOIN_TIMEOUT_MILLIS > app_config.CHECK_TIMEOUT) break;
        if (i + app_config.JOIN_LIMIT >= channels_to_join.length) {
          joined_channels = channels_to_join.length;
          break;
        };
        await new Promise(r => setTimeout(r, app_config.TWITCH_JOIN_TIMEOUT_MILLIS));

      }
      util.logTime(`${chalk.green('[FINISHED]')} Joining for ${channels_to_join.length} channels is done, managed to join ${joined_channels} channels.`, 0);

    }
    join_channels_callback();
    setInterval(join_channels_callback, app_config.CHECK_TIMEOUT);

  });

}

// const channels = fs.readFileSync('channels.txt').toString().split('\n').filter(c => c).map(c => c.trim().toLowerCase());
//
// if (!fs.existsSync('channels.txt')) return util.logTime(`${chalk.red('[ERROR]')} channels.txt doesn't exist!`);

// client.on('USERNOTICE', msg => {
//   if (msg.isSubgift() || msg.isAnonSubgift()) {
//     //you got a sub VisLaud
//     if (msg.eventParams.recipientDisplayName.toLowerCase() == config.username.toLowerCase()) {
//       util.logTime(`${chalk.hex('#F97304')('[SUB RECEIVED]')} ${chalk.red('#' + msg.channelName)} || ${chalk.yellow(msg.displayName || '👻 Anonymous')}`);
//     }
//     else {
//       util.logTime(`${chalk.green('[SUBGIFT]')} || ${chalk.red('#' + msg.channelName)} || ${chalk.yellow(msg.displayName || '👻 Anonymous')} -> ${chalk.magenta(msg.eventParams.recipientDisplayName)}`, 1);
//     }
//   }
// });

