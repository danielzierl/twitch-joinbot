const client_id = "ibux5u0hg1pufuupy4jsggbs54me8u";
const client_secret = "24a36gseh3kt8m938pdkcsfg1o79kw";
import { URLSearchParams } from "url";
import fs from "fs";


const get_oauth = async () => {
  const data = JSON.stringify({
    client_id: client_id,
    client_secret: client_secret,
    grant_type: "client_credentials",
  });
  const url = "https://id.twitch.tv/oauth2/token";
  const options = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: data,
  };
  const resp = await fetch(url, options);
  const json = await resp.json();
  return json.access_token;
};

export const get_all_livestreams_to_file = async (min_viewers) => {
  try {
    fs.copyFile(
      "./channel_lists/channels_new.txt",
      "./channel_lists/channels_old.txt",
      (err) => {
        if (err) throw err;
        console.log("channels_new.txt was copied to channels_old.txt");
      },
    );
  } catch (err) {
    console.error(err);
    return;
  }
  const token = await get_oauth();
  const livestream_names = [];
  let json = { pagination: { cursor: null } };
  while (true) {
    json = await get_next_livestreams(json.pagination.cursor, token);
    if (json.data.length == 0 || json.pagination.cursor == undefined) break;
    if (json.data[0].viewer_count < min_viewers) break;
    console.log("Viewers: " + json.data[0].viewer_count);
    json.data.forEach((livestream) =>
      livestream_names.push(sanitize_name(livestream.user_name)),
    );
    console.log("Found " + livestream_names.length + " livestreams");
  }
  write_channels_to_file("./channel_lists/channels_new.txt", livestream_names);
};
const sanitize_name = (name) => name.replace(/[^a-zA-Z0-9\_]/g, "").toLowerCase();
const get_next_livestreams = async (cursor, token) => {
  const params = new URLSearchParams({
    first: 100,
    after: cursor ?? "",
  });
  const url = "https://api.twitch.tv/helix/streams?" + params;
  const options = {
    method: "GET",
    headers: {
      Authorization: `Bearer ${token}`,
      "Client-Id": client_id,
    },
  };
  const resp = await fetch(url, options);
  const json = await resp.json();
  return json;
};
const get_timestamp_utc = () => {
  const now = new Date();
  let minutes = now.getUTCMinutes();
  let hours = now.getUTCHours();
  let day = now.getUTCDate();
  let month = now.getUTCMonth() + 1; // getUTCMonth() returns month index (0-11), so add 1
  let year = now.getUTCFullYear();
  return year + "-" + month + "-" + day + "_" + hours + ":" + minutes;
};
export function find_who_went_live(new_live_txt, old_live_txt) {
  const new_live_names = fs.readFileSync(new_live_txt).toString().split("\n");
  console.log("new live: " + new_live_names.length);
  const old_live_names = fs.readFileSync(old_live_txt).toString().split("\n");
  console.log("old live: " + old_live_names.length);
  const new_live_set = new Set(new_live_names);
  const old_live_set = new Set(old_live_names);

  const went_live = new_live_set.difference(old_live_set);
  console.log("went live: " + went_live.size);
  return Array.from(went_live);
}
export const write_channels_to_file = (file_name, channels) => {
  fs.writeFileSync(file_name, channels.join("\n"));
}
