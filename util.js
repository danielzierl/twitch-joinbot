export const leadZero = function(n) {
  return n < 10 ? "0" + n : n.toString();
};
export const dateFormat = function(date) {
  return `${leadZero(date.getDate())}/${leadZero(date.getMonth() + 1)}/${leadZero(date.getFullYear())}`;
};
export const hourFormat = function(date) {
  return `${leadZero(date.getHours())}:${leadZero(date.getMinutes())}:${leadZero(date.getSeconds())}`;
};
export const logTime = (content) => {
  let dateNow = new Date();
  console.log(
    `${dateFormat(dateNow)} ${hourFormat(dateNow)} ${content}`,
  );
};
