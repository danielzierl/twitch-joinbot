import { find_who_went_live, get_all_livestreams_to_file, write_channels_to_file } from "./all_live_getter.js";
import { init_join_client } from "./joiner.js";

const MIN_VIEWERS = 100;
const CHECK_TIMEOUT = 60000 * 5;
const JOIN_LIMIT = 19
const TWITCH_JOIN_TIMEOUT_MILLIS = 10500

const main = async () => {
  const app_config = {
    MIN_VIEWERS,
    CHECK_TIMEOUT,
    JOIN_LIMIT,
    TWITCH_JOIN_TIMEOUT_MILLIS
  }
  await recheck_live();
  setInterval(recheck_live, CHECK_TIMEOUT)
  await init_join_client("channel_lists/channels_to_join.txt", app_config);
};
const recheck_live = async () => {
  await get_all_livestreams_to_file(MIN_VIEWERS);
  const new_live = find_who_went_live(
    "channel_lists/channels_new.txt",
    "channel_lists/channels_old.txt",
  );
  write_channels_to_file("channel_lists/channels_to_join.txt", new_live);

}
main();
